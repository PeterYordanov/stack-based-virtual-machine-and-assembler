use crate::vm::vmstack::VirtualMachineStack;

const CODE_TYPE_DATA_POSITIVE: u8 = 0;
const CODE_TYPE_DATA_NEGATIVE: u8 = 3; //11

const CODE_TYPE_INSTRUCTION: u8 = 1;

pub struct VirtualMachine {
    running: bool,
    pc: usize,
    type_info: u8,
    data: i32,
    stack: VirtualMachineStack,
    code_memory: Vec<u32>,
}


impl VirtualMachine {
    pub fn new(stack_size: usize) -> VirtualMachine {
        VirtualMachine {
            code_memory: Vec::new(),
            pc: 0,
            type_info: 0,
            data: 0,
            running: true,
            stack: VirtualMachineStack::new(stack_size),
        }
    }

    fn is_running(&self) -> bool { self.running }
    fn set_running(&mut self, state: bool) { self.running = state }

    fn current_data(&self) -> i32 { self.data }
    fn current_instruction_type(&self) -> u8 { self.type_info }

    pub fn load_program(&mut self, instructions: &Vec<u32>) {
        self.code_memory.push(0xBADC0DE);
        for instruction in instructions {
            self.code_memory.push(*instruction);
        }
    }

    pub fn run(&mut self) {

        print!("Memory contents:\n[");

        for byte in &self.code_memory {
            print!("{} ", format!("{:#X}", byte));
        }

        println!("]");

        self.set_running(true);
        while self.is_running() {
            self.fetch();
            self.decode();
            self.exec();
        }
    }

    fn get_type(instruction: u32) -> u8 {
        ((instruction & 0xC0000000_u32) >> 30) as u8//2 msb
    }
    fn get_data(instruction: u32) -> i32 {
        (instruction & 0xffffffff) as i32
    }

    fn fetch(&mut self) {
        if self.pc < self.code_memory.len() {
            self.pc += 1;
        } else {
            panic!("Incomplete code execution, memory boundary reached without reading a HALT instruction\n");
        }
    }
    
    fn decode(&mut self) {
        let word = self.code_memory[self.pc];
        self.data = VirtualMachine::get_data(word);
        self.type_info = VirtualMachine::get_type(word);
    }

    fn exec(&mut self) {
        if self.current_instruction_type() == CODE_TYPE_DATA_POSITIVE || self.current_instruction_type() == CODE_TYPE_DATA_NEGATIVE {
            print!("InstructionType = Data ({} = {})\n", format!("{:#X}", self.current_instruction_type()), format!("{:#X}", self.current_data()));
            self.stack.push(self.data);
        } else {
            print!("Instruction Type Operation ({})\n", format!("{:#X}", self.current_instruction_type()));
            self.do_primitive();
        }
    }

    fn do_primitive(&mut self) {
        match self.current_data() & 0xCfffff {
            0x00000 => {
                print!("[HALT] : Stopping VM\n");
                self.set_running(false);
                return;
            }
            0x00001 => {
                let top_1 = self.stack.pop();
                let top_2 = self.stack.pop();
                print!("[ADD] : {} + {}\n", top_2, top_1);
                self.stack.push(top_1 + top_2);
            }
            0x00002 => {
                let top_1 = self.stack.pop();
                let top_2 = self.stack.pop();
                print!("[SUB] : {} - {}\n", top_2, top_1);
                self.stack.push(top_1 - top_2);
            }
            0x00003 => {
                let top_1 = self.stack.pop();
                let top_2 = self.stack.pop();
                print!("[MULT] : {} * {}\n", top_2, top_1);
                self.stack.push(top_1 * top_2);
            }
            0x00004 => {
                let top_1 = self.stack.pop();
                let top_2 = self.stack.pop();

                print!("[DIV] : {} / {}\n", top_2, top_1);
                self.stack.push(top_1 / top_2);
            }
            0x00005 => {
                let top_1 = self.stack.pop();
                let top_2 = self.stack.pop();
                print!("[XOR] : {} ^ {}\n", top_2, top_1);
                self.stack.push(top_1 ^ top_2);
            }
            0x00006 => {
                let top_1 = self.stack.pop();
                let top_2 = self.stack.pop();
                print!("[EQ] : {} == {}\n", top_2, top_1);
                self.stack.push(if top_1 == top_2 { 1 } else { 0 });
            }
            0x00007 => {
                let top_1 = self.stack.pop();
                let top_2 = self.stack.pop();
                print!("[GT] : {} > {}\n", top_2, top_1);
                self.stack.push(if top_2 > top_1 { 1 } else { 0 });
            }
            0x00008 => {
                let top_1 = self.stack.pop();
                let top_2 = self.stack.pop();
                print!("[LT] : {} < {}\n", top_2, top_1);
                self.stack.push(if top_2 < top_1 { 1 } else { 0 });
            }
            _ => {
                panic!("[Exec] : Undefined instruction\n");
            }
        }

        print!("[Top of Stack] : {}\n", self.stack.peek());
    }
}

#[cfg(test)]
mod test_vm {
    use crate::vm::vm::VirtualMachine;
    use crate::vm::vm;

    #[test]
    fn test_get_type() {
        assert_eq!(0, VirtualMachine::get_type(0x0));
        assert_eq!(vm::CODE_TYPE_INSTRUCTION, VirtualMachine::get_type(1073741825));
        assert_eq!(vm::CODE_TYPE_DATA_POSITIVE, VirtualMachine::get_type(22));
        let neg = -100;
        assert_eq!(vm::CODE_TYPE_DATA_NEGATIVE, VirtualMachine::get_type(neg as u32));
    }

    #[test]
    fn test_get_data() {
        assert_eq!(0, VirtualMachine::get_data(0));
        assert_eq!(1, VirtualMachine::get_data(1));
        let num = -1;
        assert_eq!(-1, VirtualMachine::get_data(num as u32));
    }
}