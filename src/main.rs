use clap::{App, Arg};
use byteorder::{ByteOrder, LittleEndian};
use std::fs::File;
use std::io::{Read, Write};

mod asm;
mod vm;

use crate::asm::lexer::Lexer;
use crate::asm::parser::Parser;
use crate::asm::assembler_pass::AssemblerPass;
use crate::asm::token::Token;
use crate::vm::vm::VirtualMachine;

fn main() {
    let app = App::new("assembler-and-stack-based-virtual-manager")
                        .version("1.0")
                        .about("")
                        .author("Peter Yordanov")
                        .arg(Arg::new("file")
                            .short('f')
                            .long("file")
                            .value_name("FILE")
                            .about("Input file")
                            .takes_value(true));

    let matches = app.get_matches();

    let file_path = matches.value_of("file");

    if let Some(i) = file_path {
        println!("Running file: {}", i);

        if file_path.unwrap().ends_with(".vmasm") {
            let mut input = String::new();
            File::open(file_path.unwrap())
                .expect("Failed to open file")
                .read_to_string(&mut input)
                .expect("Failed to read file");

            println!("Source code: {}", input);

            let lexer: Lexer = Lexer::new(input);
            let tokens = lexer.perform();

            let parser: Parser = Parser::new(&tokens);
            let data = parser.perform();
            let mut file = File::create("a.out").expect("Unable to create file to output");

            for i in data {
                let mut buf = [0; 4];
        
                LittleEndian::write_i32(&mut buf, i);
                file.write(&buf).expect("Unable to write to file");
            }

            let mut file = File::open("a.out").unwrap();

            let mut buf = [0; 4];
            let len = file.metadata().unwrap().len();
            let mut l = 0;
        
            let mut program: Vec<u32> = Vec::new();
        
            while l < len {
                file.read_exact(&mut buf).unwrap();
                let int = LittleEndian::read_u32(&buf[..]);
                program.push(int);
                l += 4;
            }

            let mut virtual_machine = VirtualMachine::new(10);
            virtual_machine.load_program(&program);
            virtual_machine.run();
        } else {
            panic!("Incorrect file extension");
        }
    } else {
        panic!("Provide valid file");
    }

}
