pub trait AssemblerPass<T> {
    fn perform(&self) -> T;
}