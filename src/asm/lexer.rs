use crate::asm::assembler_pass::AssemblerPass;
use crate::asm::token::Token;
use std::vec::Vec;

pub struct Lexer {
    text: String
}

impl Lexer {
    pub fn new(text: String) -> Lexer {
        Lexer {
            text: text
        }
    }
}

impl AssemblerPass<Vec<Token>> for Lexer {

    fn perform(&self) -> Vec<Token> {
        let mut vec: Vec<Token> = Vec::new();
        let mut token: Vec<char> = Vec::new();

        for ch in self.text.chars() {
            match ch {
                ' ' | '\n' => {
                    let tok: String = token.iter().cloned().collect();
                    if !tok.is_empty() && tok.len() > 0 {
                        vec.push(Token { value: tok });
                    }
                    token.clear();
                    continue;
                }
                _ => {
                    token.push(ch);
                }
            }
        }

        let tok: String = token.iter().cloned().collect();
        if !tok.is_empty() && tok.len() > 0 {
            vec.push(Token { value: tok });
        }
        vec
    }
    
}