use crate::asm::assembler_pass::AssemblerPass;
use std::vec::Vec;
use crate::asm::token::Token;

pub struct Parser<'a> {
    tokens: &'a Vec<Token>
}

impl<'a> Parser<'a> {
    pub fn new(tokens: &'a Vec<Token>) -> Parser<'a> {
        Parser {
            tokens: &tokens
        }
    }
}

pub struct Utilities;

impl Utilities {
    pub fn is_number(s: &String) -> bool {
        for i in s.chars() {
            if !i.is_digit(10) {
                return false;
            }
        }
        true
    }

    pub fn to_number(s: &String) -> i32 {
        s.parse().expect("Unable to parse to number")
    }

    pub fn map_to_number(s: &String) -> i32 {
        match s.as_str() {
            "ADD" => 0x40000001,
            "SUB" => 0x40000002,
            "MUL" => 0x40000003,
            "DIV" => 0x40000004,
            "XOR" => 0x40000005,
            "EQ" => 0x40000006,
            "GT" => 0x40000007,
            "LT" => 0x40000008,
            "NOT" => 0x40000009,
            _ => {
                panic!("Inavlid instruction");
            }
        }
    }
}

impl AssemblerPass<Vec<i32>> for Parser<'_> {

    fn perform(&self) ->  Vec<i32> {
        let mut vec: Vec<i32> = Vec::new();
        for i in self.tokens {
            if Utilities::is_number(&i.value) {
                vec.push(Utilities::to_number(&i.value));
            } else {
                vec.push(Utilities::map_to_number(&i.value));
            }
        }
        vec.push(0x40000000);
        vec
    }
    
}